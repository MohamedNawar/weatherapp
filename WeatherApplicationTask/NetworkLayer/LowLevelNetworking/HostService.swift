//
//  HostService.swift
//  WeatherApplicationTask
//
//  Created by Mohamed Hassan Nawar on 24/05/2021.
//

import Foundation

struct HostService {
    static func getBaseURL() -> String {
        return "http://api.openweathermap.org/data/2.5/"
    }
    
    static func getBaseAppId() -> String {
        return "357d5c7ec91a050ae39c35220abea93a"
    }
    
    static var headers: [String: String] { [:] }
}
