//
//  CoreNetwork.swift
//  WeatherApplicationTask
//
//  Created by Mohamed Hassan Nawar on 24/05/2021.
//

import UIKit

class CoreNetwork {
    static var sharedInstance: CoreNetworkProtocol = CoreNetwork()
    fileprivate lazy var networkCommunication: NetworkingInterface = {
        AlamofireAdaptor(baseURL: HostService.getBaseURL(), headers: HostService.headers)
    }()
}

extension CoreNetwork: CoreNetworkProtocol {
    func makeRequest<T: Codable>(request: RequestSpecs<T>,
                                 completion: @escaping (T?, Error?) -> Void) {
        networkCommunication.request(request, completionBlock: completion)
    }
}

protocol CoreNetworkProtocol {
    func makeRequest<T: Codable>(request: RequestSpecs<T>,
                                 completion: @escaping (T?, Error?) -> Void)
}
