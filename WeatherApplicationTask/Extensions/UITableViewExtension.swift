//
//  UITableViewExtension.swift
//  WeatherApplicationTask
//
//  Created by Mohamed Hassan Nawar on 24/05/2021.
//

import UIKit

extension UITableView {
    func register<T: UITableViewCell>(_ cellType: T.Type) {
        let name = String(describing: cellType)
        let nib = UINib(nibName: name, bundle: nil)
        register(nib, forCellReuseIdentifier: name)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.id, for: indexPath) as? T else {
            fatalError("\(T.self) is expected to have reusable identifier: \(T.id)")
        }
        
        return cell
    }
}

extension UITableViewCell {
    class var id: String {
        print("\(self)")
        return "\(self)"
    }
}
