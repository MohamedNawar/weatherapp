//
//  ForecastModel.swift
//  WeatherApplicationTask
//
//  Created by Mohamed Hassan Nawar on 25/05/2021.
//

import Foundation

protocol UserDefaultsWrapper {
    func fetchAllCities(completion: @escaping ([CityData]?) -> Void)
    func registerCity(name: String, temperature: String, completion: @escaping (String?) -> Void)
    func deleteCity(atRow row: Int) -> Bool
}

class ForecastData: Codable {
    var list: [ForecastInfo]?
    var city: CityData?
    var sortedCitiesList: [CityData]?
    
    func fetchAllCities(completion: @escaping ([CityData]?) -> Void) {
        let defaults = UserDefaults.standard
        if let sortedCities = defaults.object(forKey: "sortedCities") as? Data {
            let decoder = JSONDecoder()
            if let citiesList = try? decoder.decode([CityData].self, from: sortedCities) {
                completion(citiesList)
            }
        }
    }
    
    func checkIfCityIsAlreadySaved(name: String) -> Bool {
        let citiesListed = getSortedCities()
        return (citiesListed.contains(where: { $0.name == name }))
    }
    
    func getSortedCities() -> [CityData] {
        let defaults = UserDefaults.standard
        if let sortedCities = defaults.object(forKey: "sortedCities") as? Data {
            let decoder = JSONDecoder()
            if let citiesList = try? decoder.decode([CityData].self, from: sortedCities) {
                return citiesList
            }
        }
        return [CityData]()
    }
    
    func registerCity(name: String, temperature: String,
                      completion: @escaping (String?) -> Void) {
        if checkIfCityIsAlreadySaved(name: name) {
            completion("This city has already been saved")
        } else if getSortedCities().count >= 3 {
            completion("You have 3 cities saved, and you have to add up to 3 cities only")
        } else {
            storeCity(name: name, temperature: temperature)
            completion("This city has been added successfully")
        }
    }
    
    func storeCity(name: String, temperature: String) {
        let city = CityData(name: name, formattedDate: temperature)
        var sortedCities = getSortedCities()
        sortedCities.append(city)
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(sortedCities) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: "sortedCities")
        }
    }
    
    func deleteCity(atRow row: Int) -> Bool {
        sortedCitiesList = getSortedCities()
        sortedCitiesList?.remove(at: row)
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(sortedCitiesList) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: "sortedCities")
            return true
        }
        return false
    }
}

struct CityData: Codable {
    var name: String?
    var country: String?
    var formattedDate: String?
    private var date: Date?
    init(name: String, formattedDate: String) {
        self.name = name
        self.formattedDate = formattedDate
    }
}

struct ForecastInfo: Codable {
    var dt_txt: String?
    var main: ForecastMainData?
    var weather: [WeatherData]?
    var name: String?
    var sys: SysData?
}

struct SysData: Codable {
    var country: String?
}

struct ForecastMainData: Codable {
    var temp: Double?
}

struct WeatherData: Codable {
    var id: Int?
}

struct ForecastModel {
    var image: String
    var date: String
    var temperatureID: Int
    var temperature: String
    
    init(temperatureID: Int, date: String, temperature: Double) {
        self.temperatureID = temperatureID
        self.image =  updateWeatherIcon(tempreture: temperatureID)
        let selectedDate = date.toDate(format: "yyyy-MM-dd HH:mm:ss")?.asStringToDate() ?? ""
        let selectedTime = date.toDate(format: "yyyy-MM-dd HH:mm:ss")?.asStringToTime() ?? ""
        self.date = "\(selectedDate) \n \(selectedTime)"
        self.temperature = "\(Int(temperature - 273.15))"
    }
}
