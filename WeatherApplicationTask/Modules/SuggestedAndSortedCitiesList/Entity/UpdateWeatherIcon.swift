//
//  UpdateWeatherIcon.swift
//  WeatherApplicationTask
//
//  Created by Mohamed Hassan Nawar on 25/05/2021.
//

func updateWeatherIcon(tempreture: Int) -> String {
    switch tempreture {
    case 0...300 :
        return "tstorm1"
    case 301...500 :
        return "light_rain"
    case 501...600 :
        return "shower3"
    case 601...700 :
        return "snow4"
    case 701...771 :
        return "fog"
    case 772...800 :
        return "tstorm3"
    case 800, 904 :
        return "sunny"
    case 801...804 :
        return "cloudy2"
    default :
        return "tstorm3"
    }
}
