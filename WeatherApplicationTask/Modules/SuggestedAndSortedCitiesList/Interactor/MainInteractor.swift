//
//  MainInteractor.swift
//  WeatherApplicationTask
//
//  Created by Mohamed Hassan Nawar on 24/05/2021.
//

import Foundation

protocol APIServiceGetSuggestedProtocol {
    func getSuggestedCities(cityName: String,
                            completionHundler: @escaping (ForecastData?, String?) -> Void)
    func getForecastWeather(cityName: String,
                            completionHundler: @escaping (ForecastData?, String?) -> Void)
}

class MainInteractor: APIServiceGetSuggestedProtocol {
    private var coreNetwork: CoreNetworkProtocol?
    
    init(coreNetwork: CoreNetworkProtocol?) {
        self.coreNetwork = coreNetwork
    }
    
    func getSuggestedCities(cityName: String, completionHundler: @escaping (ForecastData?, String?) -> Void) {
        let parameter = ["q": cityName, "appid": HostService.getBaseAppId()] as [String: AnyObject]
        let request = RequestSpecs<ForecastData>(method: .GET, urlString: "find", parameters: parameter)
        coreNetwork?.makeRequest(request: request, completion: { model, error in
            completionHundler(model, error?.localizedDescription)
        })
    }
    
    func getForecastWeather(cityName: String, completionHundler: @escaping (ForecastData?, String?) -> Void) {
        let parameter = ["q": cityName, "appid": HostService.getBaseAppId()] as [String: AnyObject]
        let request = RequestSpecs<ForecastData>(method: .GET, urlString: "forecast", parameters: parameter)
        coreNetwork?.makeRequest(request: request, completion: { model, error in
            completionHundler(model, error?.localizedDescription)
        })
    }
}
