//
//  MainVCPresenter.swift
//  WeatherApplicationTask
//
//  Created by Mohamed Hassan Nawar on 24/05/2021.
//

import Foundation

protocol CitiesListPresenterProtocol: AnyObject {
    func didClickKeyboardCancelButton()
    func viewWillAppear()
    func viewDidLoad()
    func didSelectCity(index: Int)
    func didSelectSuggestedCityRow(index: Int)
    func didPressSearch(name: String)
    func searchTextChanged(text: String)
    func didSwipeCell(at index: Int)
    func searchBarCancelButtonClicked()
    func getForecastWeather(name: String)
}

protocol CitiesListViewProtocol: AnyObject {
    func cityNameInSearch(city: String)
    func showMessage(message: String)
    func showLoadingView()
    func hideLoadingView()
    func reloadSuggestionData()
    func reloadCitiesData()
    func showSuggetionTable()
    func hideSuggestionTable()
    func showCitiesTable()
    func updateSuggestedCityList(suggestedCities: [ForecastInfo])
    func updateSortedCitiesList(sortedCities: [CityData])
    func hideCitiesTable()
    func dismissKeyboard()
    func removeSearchbarText()
}

class MainVCPresenter: CitiesListPresenterProtocol {
    private let interactor: MainInteractor?
    private let router: MainViewRouter
    private var suggestedCities: ForecastData?
    var view: CitiesListViewProtocol?
    private(set) var cities: [CityData] = []
    private var forecastWeather: ForecastData?
    var numberOfSuggestionCities: Int {
        return suggestedCities?.list?.count ?? 0
    }
    
    init(view: CitiesListViewProtocol, router: MainViewRouter,
         interactor: MainInteractor, forecastWeather: ForecastData) {
        self.view = view
        self.router = router
        self.interactor = interactor
        self.forecastWeather = forecastWeather
    }
    
    func didPressSearch(name: String) {
        getForecastWeather(name: name)
    }
    
    func searchTextChanged(text: String) {
        if text.isEmpty {
            hideSuggestionAndShowCitiesView()
        } else {
            getForecastWeatherForCity(cityName: text)
        }
    }
    
    func viewDidLoad() {
        if UserDefaults.standard.isFirstLaunch() {
            interactor?.getForecastWeather(cityName: "cairo") { [weak self] ( forecastWeather, error ) in
                guard let self = self else { return }
                self.view?.showLoadingView()
                if let error = error {
                    self.view?.showMessage(message: error)
                } else {
                    guard let weatherData = forecastWeather else { return }
                    self.forecastWeather = weatherData
                    self.view?.hideLoadingView()
                    let temperature = forecastWeather?.list?.first?.main?.temp ?? 0
                    self.registerCity(name: "cairo", temperature: "\(Int(temperature - 273.15))")
                    forecastWeather?.fetchAllCities { [weak self] cities in
                        self?.cities = cities ?? []
                        self?.view?.updateSortedCitiesList(sortedCities: self?.cities ?? [])
                        self?.view?.reloadCitiesData()
                    }
                }
            }
        }
    }
    
    private func getForecastWeatherForCity(cityName: String) {
        interactor?.getSuggestedCities(cityName: cityName) { [weak self] (suggestedCities, error) in
            guard self != nil else { return }
            if error != nil {
                self?.view?.hideLoadingView()
                self?.view?.showMessage(message: error ?? "")
            } else {
                guard let suggestedCities = suggestedCities else { return }
                self?.suggestedCities = suggestedCities
                let count = suggestedCities.list?.count ?? 0
                if count > 0 {
                    self?.view?.showSuggetionTable()
                    self?.view?.hideCitiesTable()
                    self?.view?.updateSuggestedCityList(suggestedCities: suggestedCities.list ?? [ForecastInfo]())
                    self?.view?.reloadSuggestionData()
                } else {
                    self?.view?.hideSuggestionTable()
                    self?.view?.showCitiesTable()
                }
            }
        }
    }
    
    func viewWillAppear() {
        hideSuggestionAndShowCitiesView()
        forecastWeather?.fetchAllCities { [weak self] cities in
            self?.cities = cities ?? []
            self?.view?.updateSortedCitiesList(sortedCities: self?.cities ?? [])
            self?.view?.reloadCitiesData()
        }
    }
    
    func getForecastWeather(name: String) {
        view?.showLoadingView()
        interactor?.getForecastWeather(cityName: name) { [weak self] ( forecastWeather, error ) in
            guard let self = self else { return }
            self.view?.showLoadingView()
            if let error = error {
                self.view?.showMessage(message: error)
            } else {
                guard let weatherData = forecastWeather else { return }
                self.forecastWeather = weatherData
                self.view?.hideLoadingView()
                self.router.navigateToForecastWeatherDetailsVC(view: self.view, weatherForecast: weatherData)
            }
        }
    }
    
    func registerCity(name: String, temperature: String) {
        forecastWeather?.registerCity(name: name, temperature: temperature, completion: {  message in
            self.view?.showMessage(message: message ?? "")
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.view?.hideLoadingView()
            }
        })
        forecastWeather?.fetchAllCities { [weak self] cities in
            self?.cities = cities ?? []
            self?.view?.updateSortedCitiesList(sortedCities: self?.cities ?? [])
            self?.view?.reloadCitiesData()
        }
    }
    
    func didClickKeyboardCancelButton() {
        hideSuggestionAndShowCitiesView()
    }
    
    func searchBarCancelButtonClicked() {
        hideSuggestionAndShowCitiesView()
    }
    
    func hideSuggestionAndShowCitiesView() {
        self.view?.hideSuggestionTable()
        self.view?.showCitiesTable()
        self.view?.dismissKeyboard()
        self.view?.removeSearchbarText()
        
    }
    
    func didSelectCity(index: Int) {
        let city = cities[index].name ?? ""
        getForecastWeather(name: city)
    }
    
    func didSelectSuggestedCityRow(index: Int) {
        let city = self.suggestedCities?.list?[index].name ?? ""
        let country = self.suggestedCities?.list?[index].sys?.country ?? ""
        view?.cityNameInSearch(city: "\(city), \(country)")
        view?.hideSuggestionTable()
    }
    
    func didSwipeCell(at index: Int) {
        let result = forecastWeather?.deleteCity(atRow: index)
        if result == true {
            forecastWeather?.fetchAllCities { [weak self] cities in
                self?.cities = cities ?? []
                self?.view?.updateSortedCitiesList(sortedCities: self?.cities ?? [])
                self?.view?.reloadCitiesData()
            }
        }
    }
    
}
