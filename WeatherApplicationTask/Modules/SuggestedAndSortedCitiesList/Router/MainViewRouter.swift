//
//  MainViewRouter.swift
//  WeatherApplicationTask
//
//  Created by Mohamed Hassan Nawar on 24/05/2021.
//

import UIKit

protocol MainRouterProtocol {}

class MainViewRouter: MainRouterProtocol {
    weak var viewController: UIViewController?
    
    static func navigateModule() -> MainViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: "\(MainViewController.self)")
            as? MainViewController
        let router = MainViewRouter()
        let coreNetwork = CoreNetwork()
        let interactor = MainInteractor(coreNetwork: coreNetwork)
        let forecastWeather = ForecastData()
        let presenter = MainVCPresenter(view: view!, router: router,
                                        interactor: interactor, forecastWeather: forecastWeather)
        view!.presenter = presenter
        return view!
    }
    
    func navigateToForecastWeatherDetailsVC(view: CitiesListViewProtocol?, weatherForecast: ForecastData) {
        let detailsView = ForecastWeatherDetailsRouter.createForecastWeatherDetailsVC(weatherForecast: weatherForecast)
        if let viewController = view as? UIViewController {
            viewController.navigationController?.pushViewController(detailsView, animated: true)
        }
    }
}
