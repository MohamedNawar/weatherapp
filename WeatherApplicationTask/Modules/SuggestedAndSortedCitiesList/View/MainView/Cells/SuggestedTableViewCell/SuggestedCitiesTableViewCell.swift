//
//  SuggestedCitiesTableViewCell.swift
//  WeatherApplicationTask
//
//  Created by Mohamed Hassan Nawar on 24/05/2021.
//

import UIKit

protocol SuggestedCitiesCellView {
    func configure(cityName: String)
}

class SuggestedCitiesTableViewCell: UITableViewCell {
    @IBOutlet weak var suggestionCityLabel: UILabel!
}

extension SuggestedCitiesTableViewCell: SuggestedCitiesCellView {
    func configure(cityName: String) {
        suggestionCityLabel.text = cityName
    }
}
