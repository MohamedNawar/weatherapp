//
//  SortedCitiesTableViewCell.swift
//  WeatherApplicationTask
//
//  Created by Mohamed Hassan Nawar on 24/05/2021.
//

import UIKit

protocol SortedCitiesCellView {
    func configure(_ city: CityData)
}

class SortedCitiesTableViewCell: UITableViewCell {
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
}

extension SortedCitiesTableViewCell: SortedCitiesCellView {
    func configure(_ city: CityData) {
        dateLabel.text = city.formattedDate
        cityNameLabel.text = city.name
    }
}
