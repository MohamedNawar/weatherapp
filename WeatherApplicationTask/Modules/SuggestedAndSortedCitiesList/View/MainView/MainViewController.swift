//
//  MainViewController.swift
//  WeatherApplicationTask
//
//  Created by Mohamed Hassan Nawar on 24/05/2021.
//

import UIKit
import SKActivityIndicatorView

class MainViewController: BaseViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var citiesTableView: UITableView! {
        didSet {
            setupTableViewDelegate(tableView: citiesTableView)
            citiesTableView?.register(SortedCitiesTableViewCell.self)
        }
    }
    @IBOutlet weak var suggestionTableView: UITableView! {
        didSet {
            setupTableViewDelegate(tableView: suggestionTableView)
            suggestionTableView?.register(SuggestedCitiesTableViewCell.self)
        }
    }
    var presenter: CitiesListPresenterProtocol?
    private(set) var cities: [CityData] = []
    private(set) var suggestedCities = [ForecastInfo]()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        setupSearchBar()
        self.title = "Weather"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.viewWillAppear()
    }
}
extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    func setupTableViewDelegate(tableView: UITableView) {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = .clear
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case citiesTableView:
            return cities.count
        case suggestionTableView :
            print(suggestedCities.count)
            return suggestedCities.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(tableView)
        switch tableView {
        case citiesTableView:
            return getConfiguredCityCell(tableView: tableView, for: indexPath)
        case suggestionTableView :
            return getConfiguredSuggestionCityCell(tableView: tableView, for: indexPath)
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView,
                   commit editingStyle: UITableViewCell.EditingStyle,
                   forRowAt indexPath: IndexPath) {
        switch tableView {
        case citiesTableView:
            presenter?.didSwipeCell(at: indexPath.row)
        default:
            return
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView {
        case citiesTableView:
            presenter?.didSelectCity(index: indexPath.row)
        case suggestionTableView :
            presenter?.didSelectSuggestedCityRow(index: indexPath.row)
        default:
            return
        }
    }
    
    func getConfiguredCityCell(tableView: UITableView, for index: IndexPath) -> UITableViewCell {
        let item = cities[index.row]
        let cell: SortedCitiesTableViewCell? = tableView.dequeueReusableCell(for: index)
        cell?.configure(item)
        return cell ?? UITableViewCell()
    }
    
    func getConfiguredSuggestionCityCell(tableView: UITableView, for index: IndexPath) -> UITableViewCell {
        let cell: SuggestedCitiesTableViewCell? = tableView.dequeueReusableCell(for: index)
        let city = self.suggestedCities[index.row].name ?? ""
        let country = self.suggestedCities[index.row].sys?.country ?? ""
        cell?.configure(cityName: "\(city), \(country)")
        return cell ?? UITableViewCell()
    }
}
extension MainViewController: UISearchBarDelegate {
    func setupSearchBar() {
        searchBar.delegate = self
        searchBar.barTintColor = UIColor.clear
        searchBar.searchTextField.textColor = .white
        searchBar.backgroundColor = UIColor.clear
        searchBar.isTranslucent = true
        searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        searchBar.showsCancelButton = true
        let toolbar = UIToolbar()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                        target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: .done,
                                         target: self, action: #selector(clickCancelButtonOnKeyboard))
        toolbar.setItems([flexSpace, doneButton], animated: true)
        toolbar.sizeToFit()
        searchBar.inputAccessoryView = toolbar
    }
    
    @objc func clickCancelButtonOnKeyboard() {
        presenter?.didClickKeyboardCancelButton()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        presenter?.didPressSearch(name: searchBar.text!)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        presenter?.searchBarCancelButtonClicked()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter?.searchTextChanged(text: searchBar.text!)
    }
}
extension MainViewController: CitiesListViewProtocol {
    func showMessage(message: String) {
        SKActivityIndicator.show(message)
    }
    
    func updateSortedCitiesList(sortedCities: [CityData]) {
        self.cities = sortedCities
    }
    
    func updateSuggestedCityList(suggestedCities: [ForecastInfo]) {
        self.suggestedCities = suggestedCities
    }
    
    func dismissKeyboard() {
        self.searchBar.resignFirstResponder()
    }
    
    func removeSearchbarText() {
        self.searchBar.text = ""
    }
    func showSuggetionTable() {
        self.suggestionTableView.isHidden = false
    }
    
    func hideSuggestionTable() {
        self.suggestionTableView.isHidden = true
    }
    
    func showCitiesTable() {
        self.citiesTableView.isHidden = false
    }
    
    func hideCitiesTable() {
        self.citiesTableView.isHidden = true
    }
    
    func showLoadingView() {
        SKActivityIndicator.show()
    }
    
    func hideLoadingView() {
        SKActivityIndicator.dismiss()
    }
    
    func reloadSuggestionData() {
        self.suggestionTableView.reloadData()
    }
    
    func reloadCitiesData() {
        self.citiesTableView.reloadData()
    }
    
    func cityNameInSearch(city: String) {
        searchBar.text = city
    }
}
