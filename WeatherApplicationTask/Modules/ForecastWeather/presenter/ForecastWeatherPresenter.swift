//
//  ForecastWeatherPresenter.swift
//  WeatherApplicationTask
//
//  Created by Mohamed Hassan Nawar on 25/05/2021.
//

import Foundation

protocol ForecastWeatherView: AnyObject {
    func showMessage(message: String)
    func reloadData(forecastWeather: ForecastData)
    func showLoadingView()
    func hideLoadingView()
}
protocol ForecastWeatherPresenterProtocol {
    func saveCityData()
    func viewDidLoad()
}

class ForecastWeatherPresenter: ForecastWeatherPresenterProtocol {
    private weak var view: ForecastWeatherView?
    var forecastWeather: ForecastData!
    init(view: ForecastWeatherView,
         forecastWeather: ForecastData) {
        self.view = view
        self.forecastWeather = forecastWeather
    }
    func saveCityData() {
        self.view?.showLoadingView()
        let cityName = forecastWeather.city?.name ?? ""
        let countryName = forecastWeather.city?.country ?? ""
        let tempResult  = forecastWeather.list?.first?.main?.temp ?? 0
        let temperature = "\(Int(tempResult - 273.15) )"
        let registerNAme = "\(cityName), \(countryName)"
        forecastWeather.registerCity(name: registerNAme, temperature: temperature, completion: { [weak self] message in
            self?.view?.showMessage(message: message ?? "")
        })
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.view?.hideLoadingView()
        }
    }
    
    func viewDidLoad() {
        self.view?.reloadData(forecastWeather: forecastWeather)
    }
}
