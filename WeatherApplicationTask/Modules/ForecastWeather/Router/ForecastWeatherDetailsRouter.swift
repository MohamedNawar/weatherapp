//
//  ForecastWeatherDetailsRouter.swift
//  WeatherApplicationTask
//
//  Created by Mohamed Hassan Nawar on 25/05/2021.
//

import UIKit

class ForecastWeatherDetailsRouter {
    class func createForecastWeatherDetailsVC(weatherForecast: ForecastData) -> UIViewController {
        let storybord = UIStoryboard(name: "Forecast", bundle: nil)
        let view = storybord.instantiateViewController(withIdentifier: "\(ForecastViewController.self)")
            as? ForecastViewController
        let presenter = ForecastWeatherPresenter(view: view!, forecastWeather: weatherForecast)
        view!.presenter = presenter
        return view!
    }
}
