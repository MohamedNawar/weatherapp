//
//  CurrentWeatherForecastCell.swift
//  WeatherApplicationTask
//
//  Created by Mohamed Hassan Nawar on 25/05/2021.
//

import UIKit

protocol ForecastWeatherCellView {
    func configureForecastCell(model: ForecastModel)
}

class CurrentWeatherForecastCell: UITableViewCell {
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var temperatureImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
}

extension CurrentWeatherForecastCell: ForecastWeatherCellView {
    func configureForecastCell(model: ForecastModel) {
        dateLabel.text = model.date
        temperatureLabel.text = model.temperature
        temperatureImage.image = UIImage(named: model.image)
    }
}
