//
//  FutureWeatherForecastCell.swift
//  WeatherApplicationTask
//
//  Created by Mohamed Hassan Nawar on 25/05/2021.
//

import UIKit

class FutureWeatherForecastCell: UITableViewCell {
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var temperatureImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
}

extension FutureWeatherForecastCell: ForecastWeatherCellView {
    func configureForecastCell(model: ForecastModel) {
        dateLabel.text = model.date
        temperatureLabel.text = model.temperature
        temperatureImage.image = UIImage(named: model.image)
    }
}
