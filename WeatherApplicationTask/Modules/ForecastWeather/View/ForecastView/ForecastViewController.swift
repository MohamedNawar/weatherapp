//
//  ForecastViewController.swift
//  WeatherApplicationTask
//
//  Created by Mohamed Hassan Nawar on 25/05/2021.
//

import UIKit
import SKActivityIndicatorView

class ForecastViewController: BaseViewController {
    @IBOutlet weak var forecastTableView: UITableView! {
        didSet {
            forecastTableView.dataSource = self
            forecastTableView.delegate = self
            forecastTableView.backgroundColor = .clear
            forecastTableView?.register(CurrentWeatherForecastCell.self)
            forecastTableView?.register(FutureWeatherForecastCell.self)
        }
    }
    var presenter: ForecastWeatherPresenter!
    var forecastWeather: ForecastData?
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
    }
    
    @IBAction func saveCurrentCity(_ sender: Any) {
        presenter.saveCityData()
    }
}

extension ForecastViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecastWeather?.list?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            return configureCurrentWeatherForecastCell(tableView: tableView, index: indexPath)
        } else {
            return configureFutureWeatherForecastCell(tableView: tableView, index: indexPath)
        }
    }
    func configureCurrentWeatherForecastCell(tableView: UITableView, index: IndexPath) -> UITableViewCell {
        let currentCell: CurrentWeatherForecastCell? = tableView.dequeueReusableCell(for: index)
        currentCell?.configureForecastCell(model: getWeatherForecastModel(index: index.row))
        return currentCell ?? UITableViewCell()
    }
    
    func configureFutureWeatherForecastCell(tableView: UITableView, index: IndexPath) -> UITableViewCell {
        let futureCell: FutureWeatherForecastCell? = tableView.dequeueReusableCell(for: index)
        
        futureCell?.configureForecastCell(model: getWeatherForecastModel(index: index.row))
        return futureCell ?? UITableViewCell()
    }
    
    func getWeatherForecastModel(index: Int) -> ForecastModel {
        let temperatureID = forecastWeather?.list?[index].weather?.first?.id ?? 0
        let temperature = forecastWeather?.list?[index].main?.temp ?? 0
        let date = forecastWeather?.list?[index].dt_txt ?? ""
        let forecastModel = ForecastModel(temperatureID: temperatureID, date: date, temperature: temperature)
        return forecastModel
    }
}

extension ForecastViewController: ForecastWeatherView {
    func reloadData(forecastWeather: ForecastData) {
        self.forecastWeather = forecastWeather
        self.forecastTableView.reloadData()
    }
    
    func showMessage(message: String) {
        SKActivityIndicator.show(message)
    }
    
    func showLoadingView() {
        SKActivityIndicator.show()
    }
    
    func hideLoadingView() {
        SKActivityIndicator.dismiss()
    }
}
